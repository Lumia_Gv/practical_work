decoding_sign = {
    '---': 'O',
    '--.': 'E',
    '-.-': 'W',
    '-..': 'M',
    '.--': 'C',
    '.-.': 'A',
    '..-': 'U',
    '...': 'Q',
}


def is_cipher(_str):
    """
    Проверка последовательности на возможность расшифровки.

    Принимает на вход строку. Проверяет, все ли её символы являются '-' или '.' и передано ли правильное количество
    символов. В случае 'истины' возвращает True, иначе False.

    Args:
        _str: Последовательность символов (строка).

    Returns:
        True или False.

    Examples:
        >>> is_cipher('---...')
        True
        >>> is_cipher('----')
        False
        >>> is_cipher('abc')
        False
    """
    if all(i in ('-', '.') for i in _str) and (len(_str) % 3 == 0):
        return True
    else:
        return False


def decoding(cipher):
    """
    Декодирование последовательности.

    Принимает последовательность. Проверяет её на возможность раскодирования. В положительном случае создаёт новую
    переменную-строку, в которую последовательно добавляет раскодированные с помощью словаря символы, и возвращает
    эту переменную. В отрицательном случае возвращает строку 'impossible to decode'.

    Args:
        cipher: Последовательность символов (строка).

    Returns:
        Раскодированная строка или строка 'impossible to decode'.

    Examples:
        >>> decoding('---...')
        'OQ'
        >>> decoding('----')
        'impossible to decode'
        >>> decoding('a..')
        'impossible to decode'
    """
    if is_cipher(cipher):
        meowpher = ''
        for i in range(0, len(cipher), 3):
            meowpher += decoding_sign[cipher[i:i + 3]]
        return meowpher
    else:
        return 'impossible to decode'


if __name__ == "__main__":
    print(decoding(input('Enter the encoded string: ')))
