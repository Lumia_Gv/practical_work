import practice_3


def test_is_cipher_1():
    """Случай без ошибок"""
    assert practice_3.is_cipher('---...')


def test_is_cipher_2():
    """Случай без ошибок"""
    assert practice_3.is_cipher('-..---.-.')


def test_is_cipher_3():
    """Неверное количество символов"""
    assert not practice_3.is_cipher('----')


def test_is_cipher_4():
    """Неверное количество символов"""
    assert not practice_3.is_cipher('-...---.')

def test_is_cipher_5():
    """Прочие символы"""
    assert not practice_3.is_cipher('a..')

def test_is_cipher_6():
    """Прочие символы"""
    assert not practice_3.is_cipher('abc')


def test_decoding_1():
    """Случай без ошибок"""
    assert practice_3.decoding('---...') == 'OQ'


def test_decoding_2():
    """Случай без ошибок"""
    assert practice_3.decoding('-..---.-.') == 'MOA'


def test_decoding_3():
    """Невозможность раскодирования"""
    assert practice_3.decoding('----') == 'impossible to decode'


def test_decoding_4():
    """Невозможность раскодирования"""
    assert practice_3.decoding('-...---.') == 'impossible to decode'


def test_decoding_5():
    """Невозможность раскодирования"""
    assert practice_3.decoding('a..') == 'impossible to decode'


def test_decoding_6():
    """Невозможность раскодирования"""
    assert practice_3.decoding('abc') == 'impossible to decode'
